package com.example.quiz_5.fragments

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.quiz_5.abstacts.fragments.BaseFragment
import com.example.quiz_5.adapters.ResultAdapter
import com.example.quiz_5.databinding.FragmentResultBinding
import com.example.quiz_5.view_models.RegisterViewModel


class ResultFragment :
    BaseFragment<FragmentResultBinding, RegisterViewModel>(
        FragmentResultBinding::inflate,
        RegisterViewModel::class.java
    ) {

    private lateinit var adapter: ResultAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
    }

    private fun initRecyclerView() {
        binding.rvResult.layoutManager = LinearLayoutManager(view?.context)
        adapter = ResultAdapter()
        binding.rvResult.adapter = adapter
        viewModel.userRegisterInfoMap.observe(viewLifecycleOwner, {
            adapter.setData(it)
        })
    }
}