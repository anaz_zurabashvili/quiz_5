package com.example.quiz_5.fragments

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.quiz_5.abstacts.fragments.BaseFragment
import com.example.quiz_5.adapters.UserRegisterInfoListAdapter
import com.example.quiz_5.databinding.FragmentRegisterBinding
import com.example.quiz_5.extensions.showSnackBar
import com.example.quiz_5.utils.Strings
import com.example.quiz_5.view_models.RegisterViewModel


class RegisterFragment :
    BaseFragment<FragmentRegisterBinding, RegisterViewModel>(
        FragmentRegisterBinding::inflate,
        RegisterViewModel::class.java
    ) {

    //    protected val item: ItemViewModel by activityViewModels()
    private lateinit var adapter: UserRegisterInfoListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        binding.btnRegister.setOnClickListener { onRegisterClick() }
    }

    private fun initRecyclerView() {
        binding.rvUserRegisterList.layoutManager = LinearLayoutManager(view?.context)
        adapter = UserRegisterInfoListAdapter()
        binding.rvUserRegisterList.adapter = adapter
        viewModel.parseJson.observe(viewLifecycleOwner, {
            adapter.setData(it)
        })
    }


    private fun onRegisterClick() {
        viewModel.setMap()
        if (viewModel.getState()) {
            binding.root.showSnackBar(getString(Strings.success))
            findNavController().navigate(RegisterFragmentDirections.actionMainFragmentToResultFragment2())
        } else {
            val errorList = viewModel.getListError()
            adapter.getErrorOn(errorList!!)
            binding.root.showSnackBar(getString(Strings.try_again))
        }
    }
}
