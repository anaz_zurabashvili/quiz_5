package com.example.quiz_5.extensions

import android.view.View
import com.google.android.material.snackbar.Snackbar


fun View.showSnackBar(title: String) =
    Snackbar.make(this, title, Snackbar.LENGTH_SHORT).show()
