package com.example.quiz_5.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.quiz_5.models.UserRegisterInfo
import com.example.quiz_5.databinding.LayoutUserInfoListBinding


class UserRegisterInfoListAdapter :
    RecyclerView.Adapter<UserRegisterInfoListAdapter.ViewHolder>() {

    private val userRegisterInfoList = mutableListOf<Array<UserRegisterInfo>>()
    private var errorKeyList = mutableListOf<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutUserInfoListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) =
        holder.onBind()

    inner class ViewHolder(private val binding: LayoutUserInfoListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind() {
            initRV(binding, adapterPosition)
        }
    }

    override fun getItemCount() = userRegisterInfoList.size

    fun initRV(binding: LayoutUserInfoListBinding, position: Int) {
        binding.rvUserInfo.layoutManager = LinearLayoutManager(binding.root.context)
        val adapter = UserRegisterInfoAdapter()
        binding.rvUserInfo.adapter = adapter
        adapter.setData(userRegisterInfoList[position], position)
        if (errorKeyList.isNotEmpty() && getFilteredErrorList(userRegisterInfoList[position]).isNotEmpty())
            adapter.getErrorOn(getFilteredErrorList(userRegisterInfoList[position]), position)

    }

    fun setData(userRegisterInfoList: MutableList<Array<UserRegisterInfo>>) {
        this.userRegisterInfoList.clear()
        this.userRegisterInfoList.addAll(userRegisterInfoList)
        notifyDataSetChanged()
    }

    fun getErrorOn(errorKeyList: MutableList<Int>) {
        this.errorKeyList.clear()
        this.errorKeyList.addAll(errorKeyList)
        notifyDataSetChanged()
    }

    private fun getFilteredErrorList(adapterUserInfo: Array<UserRegisterInfo>) =
        errorKeyList.filter { it in adapterUserInfo.map { model -> model.fieldId } }.toMutableList()
}