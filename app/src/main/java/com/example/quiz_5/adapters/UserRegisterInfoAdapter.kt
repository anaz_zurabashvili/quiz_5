package com.example.quiz_5.adapters

import android.app.DatePickerDialog
import android.graphics.Color
import android.text.InputType
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.RecyclerView
import com.example.quiz_5.models.UserRegisterInfo
import com.example.quiz_5.databinding.LayoutUserInfoBinding
import com.example.quiz_5.enums.*
import com.example.quiz_5.utils.Layouts
import com.google.android.material.textfield.TextInputLayout
import java.util.*


class UserRegisterInfoAdapter :
    RecyclerView.Adapter<UserRegisterInfoAdapter.ViewHolder>() {

    private val userRegisterInfo = mutableListOf<UserRegisterInfo>()
    private var errorKeyList = mutableListOf<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutUserInfoBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int
    ) =
        holder.onBind()


    inner class ViewHolder(private val binding: LayoutUserInfoBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private lateinit var model: UserRegisterInfo

        fun onBind() {
            model = userRegisterInfo[adapterPosition]
            binding.etUserInfo.hint = model.hint.toString()
            if (model.fieldType == FieldType.INPUT.string) {
                binding.etUserInfo.inputType = when (model.keyboard) {
                    KeyBoardType.Text.string -> InputType.TYPE_CLASS_TEXT
                    else -> InputType.TYPE_CLASS_NUMBER
                }
            } else {
                binding.mTIL.endIconMode = TextInputLayout.END_ICON_DROPDOWN_MENU
                binding.etUserInfo.setBackgroundColor(Color.WHITE)
                if (model.hint == HintType.Gender.name) {
                    binding.etUserInfo.isClickable = false
                    val adapter =
                        ArrayAdapter(
                            binding.root.context, Layouts.chooser_item,
                            enumValues<Gender>().toList()
                        )
                    (binding.etUserInfo).setAdapter(adapter)
                } else {
                    getCalendarPicker(binding)
                }
            }

            if (errorKeyList.isNotEmpty() && model.fieldId in errorKeyList && model.required)
                binding.etUserInfo.error = "${model.hint}"

            binding.etUserInfo.doAfterTextChanged { text ->
                model.value = text.toString()
            }

            binding.etUserInfo.setText(model.value)
        }
    }

    override fun getItemCount() = userRegisterInfo.size

    fun setData(userRegisterInfo: Array<UserRegisterInfo>, position: Int) {
        this.userRegisterInfo.clear()
        this.userRegisterInfo.addAll(userRegisterInfo)
        notifyItemChanged(position)
    }

    fun getErrorOn(errorKeyList: MutableList<Int>, position: Int) {
        if (errorKeyList.isNotEmpty() && errorKeyList.any { it in userRegisterInfo.map { model -> model.fieldId } })
            this.errorKeyList.clear()
        this.errorKeyList.addAll(errorKeyList)
        notifyItemChanged(position)
    }

    private fun getCalendarPicker(binding: LayoutUserInfoBinding) {
        binding.etUserInfo.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(
                binding.etUserInfo.context, { _, y, m, d ->
                    val date = "$d/$m/$y"
                    binding.etUserInfo.setText(date)

                }, year, month, day
            )
            dpd.show()
        }
    }
}