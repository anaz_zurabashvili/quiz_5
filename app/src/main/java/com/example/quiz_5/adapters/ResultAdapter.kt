package com.example.quiz_5.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.quiz_5.databinding.LayoutResultTvBinding

class ResultAdapter :
    RecyclerView.Adapter<ResultAdapter.ViewHolder>() {

    private var result = mutableMapOf<Int, String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            LayoutResultTvBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(
        holder: ResultAdapter.ViewHolder,
        position: Int
    ) =
        holder.onBind()

    inner class ViewHolder(private val binding: LayoutResultTvBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            val text =
                "${result.keys.toList()[adapterPosition]} - ${result.values.toList()[adapterPosition]}"
            binding.tvResult.text = text
                
        }
    }

    override fun getItemCount() = result.size

    fun setData(result: MutableMap<Int, String>) {
        this.result = result
        notifyDataSetChanged()
    }
}