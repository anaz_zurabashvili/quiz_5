package com.example.quiz_5.abstacts.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding

typealias  Inflate<T> = (LayoutInflater, ViewGroup, Boolean) -> T
abstract class BaseFragment<VB : ViewBinding, VM : ViewModel>(
    private val inflate: Inflate<VB>,
    viewModelClass: Class<VM>
) :
    Fragment() {

    private var _binding: VB? = null
    val binding
        get() = _binding!!

    open var isSharedViewModel = true

    protected val viewModel: VM by lazy {
        if (isSharedViewModel)
            ViewModelProvider(requireActivity())[viewModelClass]
        else
            ViewModelProvider(this)[viewModelClass]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = inflate.invoke(inflater, container!!, false)
        return _binding!!.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
