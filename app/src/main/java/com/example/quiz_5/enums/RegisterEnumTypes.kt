package com.example.quiz_5.enums

enum class FieldType(val string:String) {
    INPUT("input"),
    CHOOSER("chooser"),
}

enum class HintType(val string: String) {
    UserName("UserName"),
    Email("Email"),
    Phone("phone"),
    FullName("FullName"),
    Jemali("Jemali"),
    Birthday("Birthday"),
    Gender("Gender")
}

enum class KeyBoardType (val string: String){
    Text("text"),
    Number("number")
}