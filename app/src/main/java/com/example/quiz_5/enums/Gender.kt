package com.example.quiz_5.enums

enum class Gender {
    FEMALE,
    MALE,
    OTHER
}