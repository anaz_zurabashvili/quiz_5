package com.example.quiz_5.view_models

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.quiz_5.models.UserRegisterInfo
import com.google.gson.Gson
import org.json.JSONArray

class RegisterViewModel : ViewModel() {
    private var _parseJson = MutableLiveData<MutableList<Array<UserRegisterInfo>>>()
    val parseJson: LiveData<MutableList<Array<UserRegisterInfo>>>
        get() = _parseJson

    private fun parseJson() {
        _parseJson.value =
            Gson().fromJson(jsonObject.toString(), Array<Array<UserRegisterInfo>>::class.java)
                .toMutableList()
    }

    private var _userRegisterInfoMap = MutableLiveData<MutableMap<Int, String>>()
    val userRegisterInfoMap: LiveData<MutableMap<Int, String>>
        get() = _userRegisterInfoMap

    fun getState() =
        _userRegisterInfoMap.value?.values?.filter { it.isNotEmpty() }?.size == _userRegisterInfoMap.value!!.size

    fun getListError() =
        _userRegisterInfoMap.value?.filterValues { it.isEmpty() }?.keys?.toMutableList()

    fun setMap() {
        parseJson.value!!.forEach { arrayList ->
            arrayList.forEach { userRegisterInfo ->
                if (!userRegisterInfo.value.isNullOrBlank() || userRegisterInfo.required)
                    _userRegisterInfoMap.value?.set(
                        userRegisterInfo.fieldId,
                        userRegisterInfo.value ?: ""
                    )
            }
        }
    }

    private val jsonObject = JSONArray(
        "[\n" +
                "  [\n" +
                "    {\n" +
                "      \"field_id\": 1,\n" +
                "      \"hint\": \"UserName\",\n" +
                "      \"field_type\": \"input\",\n" +
                "      \"keyboard\": \"text\",\n" +
                "      \"required\": false,\n" +
                "      \"is_active\": true,\n" +
                "      \"icon\": \"https://jemala.png\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"field_id\": 2,\n" +
                "      \"hint\": \"Email\",\n" +
                "      \"field_type\": \"input\",\n" +
                "      \"required\": true,\n" +
                "      \"keyboard\": \"text\",\n" +
                "      \"is_active\": true,\n" +
                "      \"icon\": \"https://jemala.png\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"field_id\": 3,\n" +
                "      \"hint\": \"phone\",\n" +
                "      \"field_type\": \"input\",\n" +
                "      \"required\": true,\n" +
                "      \"keyboard\": \"number\",\n" +
                "      \"is_active\": true,\n" +
                "      \"icon\": \"https://jemala.png\"\n" +
                "    }\n" +
                "  ],\n" +
                "  [\n" +
                "    {\n" +
                "      \"field_id\": 4,\n" +
                "      \"hint\": \"FullName\",\n" +
                "      \"field_type\": \"input\",\n" +
                "      \"keyboard\": \"text\",\n" +
                "      \"required\": true,\n" +
                "      \"is_active\": true,\n" +
                "      \"icon\": \"https://jemala.png\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"field_id\": 14,\n" +
                "      \"hint\": \"Jemali\",\n" +
                "      \"field_type\": \"input\",\n" +
                "      \"keyboard\": \"text\",\n" +
                "      \"required\": false,\n" +
                "      \"is_active\": true,\n" +
                "      \"icon\": \"https://jemala.png\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"field_id\": 89,\n" +
                "      \"hint\": \"Birthday\",\n" +
                "      \"field_type\": \"chooser\",\n" +
                "      \"required\": false,\n" +
                "      \"is_active\": true,\n" +
                "      \"icon\": \"https://jemala.png\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"field_id\": 898,\n" +
                "      \"hint\": \"Gender\",\n" +
                "      \"field_type\": \"chooser\",\n" +
                "      \"required\": false,\n" +
                "      \"is_active\": true,\n" +
                "      \"icon\": \"https://jemala.png\"\n" +
                "    }\n" +
                "  ]\n" +
                "]\n"
    )

    init {
        parseJson()
        _userRegisterInfoMap.value = mutableMapOf()
    }
}
