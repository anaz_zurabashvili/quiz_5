package com.example.quiz_5.models

import com.google.gson.annotations.SerializedName

data class UserRegisterInfo(
    @SerializedName("field_id")
    val fieldId: Int,
    val hint: String?,
    @SerializedName("field_type")
    val fieldType: String?,
    val keyboard: String?,
    val required: Boolean,
    @SerializedName("is_active")
    val isActive: Boolean,
    val icon: String?,
    var value: String?,
)

